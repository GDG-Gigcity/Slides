package main

import "fmt"

func main() {
	fmt.Println(add(42, 13))
}

// START OMIT
func add(x int, y int) int {
	return x + y
}

// END OMIT
