package main

import (
	"fmt"
	"os"

	"github.com/golang/glog"
)

func main() {
	glog.Infoln("Printing Hello, World")
	fmt.Println("Hello, World!")
	os.Exit(0)
}
