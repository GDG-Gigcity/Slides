package exp

import "fmt"

func Foo() {
	fmt.Println("I am exported")
}

func bar() {
	fmt.Println("I am not")
}
