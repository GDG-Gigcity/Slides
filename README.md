# Google Developer Group Gigcity presentations

This repository is for the presentation files for GDG Gigcity.

## Getting started

The files in this repository is just the presentation file, and to be of any
use the Go Present tool is required.  The present tool requires that you have
at least [Go version 1 installed](http://golang.org/doc/install).

### Installing Go Present tool

With Go installed use the /go get/ tool grab the latest version from the
sub-repo:

    go get golang.org/x/tools/present

That will download and install the present tool.

### Usage

With the present tool built it is easy to test out the presentation files with:

    $ present /path/to/presentation

## Writing presentation files

The syntax for writing presentation files is documented at
[GoDoc.org](http://godoc.org/code.google.com/p/go.tools/present)

# License

Except where otherwise noted this work is licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License. To view a copy of this
license, visit http://creativecommons.org/licenses/by-sa/4.0/deed.en_US.

