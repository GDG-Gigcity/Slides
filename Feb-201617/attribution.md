# Attribution

This slide has been forked from Dave Cheney's Go 1.6 slides for Go Sydney users'
group.  The original slides can be found at
https://github.com/davecheney/gosyd/blob/master/go1.6.slide
