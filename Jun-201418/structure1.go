package main

import "fmt"

// Location stores the Latatude and Longitude for a given
// point in space.
type Location struct {
	Lat  float64
	Long float64
}

// Format formats the Lat and Long values in the Location tye for printing
func (l Location) Format() string {
	return fmt.Sprintf("%f° N, %f° W", l.Lat, l.Long)
}

func main() {
	var l Location
	l.Lat, l.Long = 35.0456, 85.2672
	fmt.Println(l.Format())
}
