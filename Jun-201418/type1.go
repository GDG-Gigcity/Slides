package main

import "fmt"

func main() {
	var s1 string
	s1 = "Hello, World!"
	fmt.Printf("Type of s1: %T\n", s1)

	s2 := "हेलो विश्व"
	fmt.Printf("Type of s2: %T\n", s2)

	s3 := '⌘'
	fmt.Printf("Type of s3: %T\n", s3)
}
