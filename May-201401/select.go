package main

import (
	"fmt"
	"time"
)

var c chan int

func main() {
	c = make(chan int)
	var i int
	go ready("Tea", 2)
	go ready("Coffee", 1)
	fmt.Println("I'm waiting")
L:
	for {
		select {
		case <-c:
			i++
			if i > 1 {
				break L
			}
		}
	}
}

func ready(w string, sec int) {
	time.Sleep(time.Duration(sec) * time.Second)
	fmt.Println(w, "is ready!")
	c <- 1
}
